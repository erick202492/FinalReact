import React from "react";
import Axios from "axios";
import Container from "@material-ui/core/Container";
import { makeStyles } from '@material-ui/core/styles';
import { List, Avatar, Input, Button, Switch } from "antd";
import "antd/dist/antd.css";

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const data = [];

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
    maxHeight: 400,
    border:"1px dotted",
  },
  media: {
    height: 200,
  },
});

export default class BookList extends React.Component {
  //constructor(props)
  state = {
    //   super(props);
    data,
    book: 
    {
       id: "", 
       title: "", 
       author: "", 
       published_date: "", 
       pages: "", 
       language: "", 
       publisher_id: "", 
       comens: [{ 
         comen: "", 
         user: {
            username: "" 
          } 
        }] 
      },
    add: {
	avatar:"",
    username: `${sessionStorage.getItem('USERNAME')}`,
    comen: "",
    bookId: `${this.props.match.params.id}`,
    checked: true
},   
};

onInputChange = (e, type) => {
  console.log("add ahh");
  
  const add = { ...this.state.add };
  add[type] = e.target.value;
  this.setState({ add });
};

onAddContact = () => {
	
    
  
  
  const { book, add } = this.state;
  if (!add.username) return;
  add.avatar = `https://api.adorable.io/avatars/96/${add.username.replace(
    / +/,
    ""
  )}@adorable.png`
  add.username = add.username
    .split(" ")
    .map(str => str[0] && str[0].toUpperCase() + str.slice(1))
    .join(" ");
  add.comen = add.comen[0].toUpperCase() + add.comen.slice(1);
  add.user = {username: add.username}
  console.log(add)
  Axios
  .post(`http://localhost:4001/api/comment/${this.props.match.params.id}`, add, {
    headers: {
      Authorization: sessionStorage.getItem("TOKEN")
    }
  })
  .then(function(response) {
    console.log(response)
    //console.log(response.data);
    // sessionStorage.setItem('TOKEN', response.data.accessToken);
  })
  .catch(function(error) {
    console.log(error);
  });
  this.setState({ 
    book: {...book, comens:[...book.comens, add]}
    
  });
  this.setState({
    add: {	
    avatar: "",
    username: `${sessionStorage.getItem('USERNAME')}`,
    comen: "",
    //checked: true
   }
  });
};

onSwitchOn = checked => {
  const add = { ...this.state.add };
  add.checked = checked;
  this.setState({ add });
};

componentDidMount() {
  if (sessionStorage.getItem("TOKEN") === null) {
    alert("you must login to show this page")
    window.location = "/";
  }
  Axios.get(
    `http://localhost:4001/api/detailbooks/${this.props.match.params.id}`,
    {
      headers: {
        "Authorization": sessionStorage.getItem('TOKEN')
      }
    }
  )
    .then((response) => {
      var book = response.data;
      console.log(JSON.stringify(book));
      if (!book) {
        alert("Please Login to View Books")
      } else {
        this.setState({ book: book.data[0] });
      }
    }
      ,
      (error) => {
        //console.log(JSON.stringify(error.response.data.message))
        // console.log(response)
        // var status = error.response

      }
    );
}

// componentWillMount(){
//
// }

// updateClock()
// {
//     this.setState({
//         date: new Date()
//     })
// }

render() {
  const { data, add, book } = this.state;
  const { username, comen } = add;
  return (
  
    <Container component="main" maxWidth="sm">
      <div>

      <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="140"
          image="https://i.ytimg.com/vi/C-o060BUeKo/maxresdefault.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          {book.title}
          
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Author : {book.author} <br />
            Publish Date : {book.published_date}<br />
            Pages : {book.pages}<br />
            Language : {book.language}<br />
            Publihser : {book.publisher_id}<br />
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
       
      </CardActions>

        <List
          itemLayout="horizontal"
          dataSource={book.comens}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar src={item.avatar} />}
                title={item.user.username}
                description={item.comen}
              />
              
            </List.Item>
          )}
          style={{
            width: "100%"
          }}
        />
        <form className="CommentList__form" autoComplete="off">
          <div className="CommentList__input">
            <label htmlFor="name">Name</label>
            <Input disabled
              value={username}
              placeholder="Your beautiful name here"
              id="name"
              onChange={e => this.onInputChange(e, "username")}
            />
          </div>
          <div className="CommentList__input">
            <label htmlFor="comen">comen</label>
            <Input
              value={comen}
              placeholder="Your beautiful comen here"
              id="comen"
              onChange={e => this.onInputChange(e, "comen")}
            />
          </div>
          <div className="CommentList__add">
            <Button
              onClick={this.onAddContact}
              style={{ marginRight: "1em" }}
              disabled={!add.username}
            >
              Add
            </Button>
            <Switch
              defaultChecked
              checkedChildren="👦"
              onChange={this.onSwitchOn}
              style={{ minWidth: "48px" }}
            />
          </div>
        </form> 
      </div>
      <>

      </>      
    </Container>
  );
}
}
