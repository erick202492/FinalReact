import React from "react";
import Container from "@material-ui/core/Container";

const usersgreetings = props => (
<Container maxWidth="sm">
  <h2 className="bg-primary text-white text-center p-2">Welcome User, {sessionStorage.getItem("USERNAME")}</h2>
</Container>
);

export default usersgreetings