import React, { useState, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import axios from 'axios';
import Container from "@material-ui/core/Container";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: "left"

  },
  card: {
    maxWidth: 345,
    maxHeight: 500,
    border:"1px dotted",
    margin:10,
    
  },
  media: {
    height: 140,
  },
  gridList: {
    width: 800,
    height: 550,
  },
  icon: {
    color: 'rgba(255, 255, 255, 255)',
  },
}));

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileData = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */



export default function TitlebarGridList() {

  const [data, setData] = useState({
  book : [{
  id: "",
	title: "",
	author: "",
	published_date: "",
	pages: "",
	language: "",
	publisher_id: "", 
    }
	]
  });

  useMemo(() => {
    const fetchData = async () => {
      const result = await axios({
        method: 'get', //you can set what request you want to be
        url: 'http://localhost:4001/api/books/',
        headers: {"Authorization": sessionStorage.getItem('TOKEN')}
      });
      //console.log(result.data.data)
      setData({book : result.data.data});
    };
    fetchData();
    console.log(data);
  }, []);
  console.log(data);
  const mapping = data.book.map((data) => data.id )
  console.log(mapping)
  
  const classes = useStyles();

  return (
  <Container className={classes.root}>
     
      {data.book.map(books => (       
      <Card className={classes.card} key={books.id}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image="https://i.ytimg.com/vi/C-o060BUeKo/maxresdefault.jpg"
          title="List Books"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          {books.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          This Books Created by {books.author}, has been published at {books.published_date} 
          the books use {books.language} language, contains  {books.pages} pages, and published by  {books.publisher_id}   
          
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
        <a href={`/userpanel/booksid/${books.id}`}>Detail</a>
        </Button>
      </CardActions>
      
    </Card>
    ))}
    </Container>
    
  );
}
