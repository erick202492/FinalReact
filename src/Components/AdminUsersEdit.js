import React, { useState, useEffect } from 'react'
import Container from "@material-ui/core/Container";

const EditUserForm = props => {
  const [ user, setUser ] = useState(props.currentUser)

  useEffect(
    () => {
      setUser(props.currentUser)
    },
    [ props ]
  )
  // You can tell React to skip applying an effect if certain values haven’t changed between re-renders. [ props ]

  const handleInputChange = event => {
    const { name, value } = event.target
    setUser({ ...user, [name]: value })
  }

  return (
  <Container component="main">
    <form
      onSubmit={event => {
        event.preventDefault()

        props.updateUser(user.id, user)
      }}
    >
      <label>Name</label>
      <input type="text" name="name" value={user.name} onChange={handleInputChange} />
      <label>Username</label>
      <input type="text" name="username" value={user.username} onChange={handleInputChange} />
      <label>Email</label>
      <input type="text" name="email" value={user.email} onChange={handleInputChange} />
      <button>Update user</button>
      <button onClick={() => props.setEditing(false)} className="button muted-button">
        Cancel
      </button>
    </form>
    </Container>
  )
}

export default EditUserForm

// import React from 'react';
// import Button from '@material-ui/core/Button';
// import TextField from '@material-ui/core/TextField';
// import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
// import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
// import DialogTitle from '@material-ui/core/DialogTitle';
// import { useForm } from 'react-hook-form'
// import Grid from "@material-ui/core/Grid";


// export default function UserEdit() {

//   const [open, setOpen] = React.useState(false);
//   const handleClickOpen = () => {
//     setOpen(true);
//   };

//   const handleClose = () => {
//     setOpen(false);
//   };
//   // add users form
//   const { register, handleSubmit, errors } = useForm()
//   const onSubmit = data =>
//     console.log(data)

//   return (
//     <div>
//       <Button variant="outlined" color="primary" onClick={handleClickOpen}>
//         Edit Users
//       </Button>
      
//       <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
//         <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
//         <DialogContent>
//           <DialogContentText>
//             To subscribe to this website, please enter your email address here. We will send updates
//             occasionally.
//           </DialogContentText>
//           <form
//             onSubmit={handleSubmit(onSubmit)}
//             noValidate
//           >
//             <Grid container spacing={2}>
//               <Grid item xs={12} sm={6}>
//                 <TextField
//                   autoComplete="fname"
//                   name="name"
//                   variant="outlined"
//                   required
//                   fullWidth
//                   id="firstName"
//                   label="Name"
//                   // value="namesss"
//                   autoFocus
//                   error={!!errors.name}
//                   inputRef={register({
//                     required: true
//                   })}
//                 />
//                 {errors.name && "name required"}
//                 <br />
//               </Grid>
//               <Grid item xs={12} sm={6}>
//                 <TextField
//                   variant="outlined"
//                   required
//                   fullWidth
//                   id="lastName"
//                   label="Username"
//                   name="username"
//                   // value="v- usernamecccccccc"
//                   autoComplete="lname"
//                   error={!!errors.username}
//                   inputRef={register({
//                     required: true
//                   })}
//                 />
//                 {errors.username && "username required"}
//                 <br />
//               </Grid>
//               <Grid item xs={12}>
//                 <TextField
//                   variant="outlined"
//                   required
//                   fullWidth
//                   id="email"
//                   label="Email Address"
//                   name="email"
//                   // value="cccccc@email.com"
//                   autoComplete="email"
//                   error={!!errors.email}
//                   inputRef={register({
//                     required: true,
//                     pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
//                   })}
//                 />
//                 {errors.email && "Invalid email address"}
//                 <br />
//               </Grid>
//               <Grid item xs={12}>
//                 <TextField
//                   variant="outlined"
//                   required
//                   fullWidth
//                   name="password"
//                   label="Password"
//                   type="password"
//                   id="password"
//                   autoComplete="current-password"
//                   // value="password"
//                   error={!!errors.password}
//                   inputRef={register({
//                     required: true
//                   })}
//                 />
//                 {errors.password && "name required"}
//                 <br />
//               </Grid>
//               <Grid item xs={12}>

//                 {/* <lable className="reactSelectLabel">React select</lable>
//             <Select
//             className="reactSelect"
//             name="filters"
//             placeholder="Filters"
//             value="USER"
//             selected="selected"
//             options={options}
//             onChange={handleMultiChange}
//             isMulti
//             /> */}
//                 <div style={{ display: 'none' }}>
//                   <select name='roles' ref={register({ required: true })} multiple>
//                     <option value='user' selected='selected'></option>
//                   </select>
//                 </div>
//                 {errors.roles && "name required"}
//                 <br />

//               </Grid>
//             </Grid>
//             <Button
//               type="submit"
//               fullWidth
//               variant="contained"
//               color="primary"

//             >
//               Sign Up
//           </Button>
//             <Grid container justify="flex-end">
//               <Grid item>

//               </Grid>
//             </Grid>
//           </form>
//         </DialogContent>
//         <DialogActions>
//           <Button onClick={handleClose} color="primary">
//             Cancel
//           </Button>
//           <Button onClick={handleClose} color="primary">
//             Subscribe
//           </Button>
//         </DialogActions>
//       </Dialog>

      
//       <p>ini adalah tabel</p>
//     </div>


//   );
// }
