import React, { useState, useMemo } from 'react';
import axios from 'axios';

function FetchTest() {
    const [book, setBook] = useState({
        id: '',
        title: 'titleyaa',
        author: '',
        published_date: '',
        pages: '',
        language: '',
        publisher_id: '',
        createdAt: '',
        updatedAt: ''
    });

    useMemo(() => {
        const fetchData = async () => {
            const result = await axios({
                method: 'get', //you can set what request you want to be
                url: 'http://localhost:4001/api/books/',
                headers: { "Authorization": sessionStorage.getItem('TOKEN') }
            });
            //console.log(result.data.data[0])
            setBook(result.data.data[0]);
        };
        fetchData();
        //console.log(data.id);
    }, []);
    console.log(book);


    return (
        <div>
            {book.title}
        </div>
    );
}

export default FetchTest;