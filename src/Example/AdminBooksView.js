import React from "react";
import Axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default class BookList extends React.Component {
    //constructor(props)
    state = {
        //   super(props);
        book: []

    };


    componentDidMount() {
        if (sessionStorage.getItem("TOKEN") === null) {
            alert("you must login to show this page")
            window.location = "/login";
        }
        //this.timerID = setInterval(() => this.updateClock(), 1000);
        // Axios.get("http://localhost:4000/api/books/").then(res => {
        //   const book = res.data;
        //   this.setState({ book : book.data });
        //   console.log(res.status)
        // });
        Axios.get(
            'http://localhost:4001/api/books/',
            {
                headers: {
                    "Authorization": sessionStorage.getItem('TOKEN')
                }
            }
        )
            .then((response) => {
                var book = response.data;
                console.log(response.data);
                if (!book) {
                    alert("Please Login to View Books")
                } else {
                    this.setState({ book: book.data });
                }
            }
                ,
                (error) => {
                    //console.log(JSON.stringify(error.response.data.message))
                    // console.log(response)
                    // var status = error.response

                }
            );
    }

    // componentWillMount(){
    //     clearInterval(this.timerID)
    // }

    // updateClock()
    // {
    //     this.setState({
    //         date: new Date()
    //     })
    // }

    render() {
        return (
            

               <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>id</TableCell>
                                <TableCell align="right">title</TableCell>
                                <TableCell align="right">author</TableCell>
                                <TableCell align="right">publish date</TableCell>
                                <TableCell align="right">pages</TableCell>
                                <TableCell align="right">language</TableCell>
                                <TableCell align="right">publisher id</TableCell>
                                <TableCell align="right">edit</TableCell>
                                <TableCell align="right">delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.book.map(books => (
                                <TableRow key={books.id}>
                                    <TableCell component="th" scope="row">
                                        {books.id}
                                    </TableCell>
                                    <TableCell align="right">{books.title}</TableCell>
                                    <TableCell align="right">{books.author}</TableCell>
                                    <TableCell align="right">{books.published_date}</TableCell>
                                    <TableCell align="right">{books.pages}</TableCell>
                                    <TableCell align="right">{books.language}</TableCell>
                                    <TableCell align="right">{books.publisher_id}</TableCell>
                                    <TableCell align="right"><a href={"http://localhost:3000/axios/editbook/" + books.id}>Edit</a></TableCell>
                                    <TableCell align="right"><a href={"http://localhost:3000/axios/deletebook/" + books.id}>Delete</a></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
         
        );
    }
}
