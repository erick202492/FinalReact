import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import axios from "axios";
import { useForm, watch } from "react-hook-form";
import { Input } from "@material-ui/core";


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function SignUp() {

  const classes = useStyles();

  const { register, handleSubmit, setValue, errors } = useForm();
  const onSubmit = (data, e) => {
    e.preventDefault();
    alert(JSON.stringify(data, null, 2));
    console.log(data);

    axios
      .post("http://localhost:4001/api/books", data, {
        headers: {
          Authorization: sessionStorage.getItem("TOKEN")
        }
      })
      .then(function(response) {
        console.log(response);
        if (response.status === 201) {
          alert(response.data.status);
          window.location = "/login";
        }
        //console.log(response.data);
        // sessionStorage.setItem('TOKEN', response.data.accessToken);
      })
      .catch(function(error) {
        console.log(error);
      });
    //props.history.push("/");
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
         <Button variant="contained" color="secondary">View Books</Button> 
        </Typography>
        <form
          className={classes.form}
          onSubmit={handleSubmit(onSubmit)}
          noValidate
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="title"
                name="title"
                variant="outlined"
                required
                fullWidth
                id="title"
                label="title"
                // value="namesss"
                autoFocus
                error={!!errors.title}
                inputRef={register({
                  required: true
                })}
              />
              {errors.title && "title required"}
              <br />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="author"
                label="author"
                name="author"
                // value="v- usernamecccccccc"
                autoComplete="lname"
                error={!!errors.author}
                inputRef={register({
                  required: true
                })}
              />
              {errors.author && "author required"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="publisherdate"
                id="date"
                label="Publisher Date"
                type="date"
                defaultValue=""
                error={!!errors.publisherdate}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
                inputRef={register({
                  required: true
                })}
              />
              {errors.publisherdate && "PublisherDate required"}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="pages"
                label="pages"
                type="number"
                id="pages"
                autoComplete="pages"
                // value="password"
                error={!!errors.pages}
                inputRef={register({
                  required: true
                })}
              />
              {errors.pages && "Pages required"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="language"
                label="Language"
                type=""
                id="language"
                autoComplete="language"
                // value="password"
                error={!!errors.language}
                inputRef={register({
                  required: true
                })}
              />
              {errors.language && "language required"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="publisher_id"
                label="Publiher"
                type=""
                id="publisher_id"
                autoComplete="publisher_id"
                // value="password"
                error={!!errors.publisher_id}
                inputRef={register({
                  required: true
                })}
              />
              {errors.publisher_id && "publisher id"}
              <br />
            </Grid>
            <Grid item xs={12}>
              {/* <lable className="reactSelectLabel">React select</lable>
            <Select
            className="reactSelect"
            name="filters"
            placeholder="Filters"
            value="USER"
            selected="selected"
            options={options}
            onChange={handleMultiChange}
            isMulti
            /> */}
              <div style={{ display: "none" }}>
                <select
                  name="roles"
                  ref={register({ required: true })}
                  multiple
                >
                  <option value="user" selected="selected"></option>
                </select>
              </div>
              {errors.roles && "name required"}
              <br />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Add Books
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/axios/viewbook" variant="body2">
                View Books
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
